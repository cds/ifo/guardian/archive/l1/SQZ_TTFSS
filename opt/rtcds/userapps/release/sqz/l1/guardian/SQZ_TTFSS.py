# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_TTFSS.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/l1/guardian/SQZ_TTFSS.py $

# TODO:
### 1
# look at frequency counter logic:
#   delta_freq = ezca['SQZ-VCO_FREQUENCY'] - 2 * ezca['IMC-VCO_FREQUENCY']
#
### 2
#

#import sys
import time

# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig

from guardian import (
    GuardState,
    GuardStateDecorator,
)

from guardian.state import (
    TimerManager,
)


#from guardian.ligopath import userapps_path
#from subprocess import check_output
#import time
#import cdsutils
#import sys

from ezca_automon import (
    EzcaEpochAutomon,
    EzcaUser,
    deco_autocommit,
    SharedState,
    DecoShared,
)

# this is to keep emacs python-mode happy from lint errors due
# to the namespace injection that Guardian does
if False:
    IFO = None
    ezca = None
    notify = None
    log = None

if sqzconfig.nosqz:
#    nominal = 'IDLE'
    nominal = 'LOCKED'
else:
    nominal = 'LOCKED'
#nominal = 'LOCKED'
#nominal = 'IDLE'

class TTFSSParams(EzcaUser):
    # these rails are for the TTFSS fast output and EOM RMS
    PZT_OUT_RAIL_HIGH = 9
    PZT_OUT_RAIL_LOW  = -3
    PZT_OUT_RAIL_HIGH_WARN = 8
    PZT_OUT_RAIL_LOW_WARN  = -2
    EOM_RMS_RAIL = 5
    EOM_RMS_WARN = 1
    # 0.3 changed AE 20190224  # was 0.02... what happened? # EOM RMS is often at 0.2 level. common gain too high?
    # 0.1 changed MN 20220628 nominally 0.06
    # 0.1 changed MN 20221207 nominally 0.3. FC/LO loop makes the EOM RMS bigger

    PZT_OUT_LAST = 0.0

    # counter for lock attempts
    LOCK_ATTEMPT_COUNTER = 0
    LOCK_ATTEMPT_MAX = 5

    # slow servo limits
    SLOW_MIN = -0.20
    SLOW_MAX =  0.20
    SLOW_DELTA_MAX = 0.002

    # PZT tracking slow loop
    SLOW_GAIN_NOM           = -0.003

    # frequency tracking slow loop
    SLOW_TRACK_GAIN_NOM    = -3e-10

    FAULT_TIMEOUT_S        = 1
    UNLOCK_TIMEOUT_S       = 1

    intervention_msg = None

    def __init__(self):
        self.timer = TimerManager(logfunc = None)
        self.timer['FAULT_TIMEOUT'] = 0
        self.timer['UNLOCK_TIMEOUT'] = 0

    def update_calculations(self):
        """
        No calculations, but keeping forwards compatible if they are wanted
        """
        return

    def fault_checker(self):
        fault_condition = False

        # check power on TTFSS PD
        if ezca['SQZ-FIBR_PD_DC_POWER'] < ezca['SQZ-TTFSS_GRD_DC_MIN']:
            notify("Not enough power on FIBR PD")
            fault_condition = True

        if ezca['SQZ-FIBR_SERVO_DEMOD_RFMON'] < ezca['SQZ-TTFSS_GRD_RF_MIN']:
            notify("Not enough RF on FIBR PD (1611) to lock")
            fault_condition = True

        if not ezca['PSL-FSS_AUTOLOCK_STATE'] == 4:
            notify("FSS is not locked.")
            fault_condition = True

        if fault_condition:
            return True
            
        return False

    def check_unlocked(self):
        unlock_condition = False

        # if the output is railed, we are not locked
        if ezca['SQZ-FIBR_SERVO_FASTMON'] < self.PZT_OUT_RAIL_LOW:
            notify("PZT OUT RAILED LOW")
            unlock_condition = True
        elif ezca['SQZ-FIBR_SERVO_FASTMON'] < self.PZT_OUT_RAIL_LOW_WARN:
            notify("PZT OUT NEAR LOW RAIL")
        elif ezca['SQZ-FIBR_SERVO_FASTMON'] > self.PZT_OUT_RAIL_HIGH:
            notify("PZT OUT RAILED HIGH")
            unlock_condition = True
        elif ezca['SQZ-FIBR_SERVO_FASTMON'] > self.PZT_OUT_RAIL_HIGH_WARN:
            notify("PZT OUT NEAR HIGH RAIL")

        #TODO Do we need this? Seems OK if it jumps as long as it isn't otherwise bad
       # if abs(ezca['SQZ-FIBR_SERVO_FASTMON'] - self.PZT_OUT_LAST) > 2.0 and self.PZT_OUT_LAST != 0.0:
       #     notify("PZT OUT JUMPED")
       #     unlock_condition = True

        # check EOM RMS for problems
        if ezca['SQZ-FIBR_SERVO_EOMRMSMON'] > self.EOM_RMS_RAIL:
            notify("EOM RMS RAILED")
            unlock_condition = True
        elif ezca['SQZ-FIBR_SERVO_EOMRMSMON'] > self.EOM_RMS_WARN:
            notify("EOM RMS too big... Oscillating?")

        # look at frequency counters
        #delta_freq = ezca['SQZ-VCO_FREQUENCY'] - 2 * ezca['IMC-VCO_FREQUENCY']
        # if abs(delta_freq) > ezca['SQZ-TTFSS_GRD_TRACK_DELTA_FREQ_MAX']:
        #   notify("TTFSS VCO frequency too from from PSL")
        #   log(delta_freq)
        #   return False

        if unlock_condition:
            return True
        return False


class TTFSSShared(SharedState):
    @DecoShared
    def ttfss(self):
        EzcaEpochAutomon.monkeypatch_subclass_instance(ezca)
        return TTFSSParams()


shared = TTFSSShared()


#############################################
#Functions

def down_state_set():
    # reset all to down state
    ezca['SQZ-FIBR_SERVO_COMGAIN'] = ezca['SQZ-TTFSS_GRD_COMGAIN_ACQ']
    ezca['SQZ-FIBR_SERVO_BOOST1EN'] = 1
    ezca['SQZ-FIBR_SERVO_BOOST2EN'] = 0
    ezca['SQZ-FIBR_SERVO_SPARE1'] = 0
    ezca['SQZ-FIBR_SERVO_BOOST3EN'] = 0
    ezca['SQZ-FIBR_SERVO_RAMPEN'] = 1
    ezca['SQZ-FIBR_SERVO_EOMEN'] = 0
    ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 'Off'


#############################################
#Decorator

# There should be at least two types of fault_checker here.
# One you go back to trying again (lock loss), another you just go straight to down
# (no laser beam).

class lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if shared.ttfss.check_unlocked():
            if shared.ttfss.timer['UNLOCK_TIMEOUT']:
                return 'LOCKLOSS'
        else:
            shared.ttfss.timer['UNLOCK_TIMEOUT'] = shared.ttfss.UNLOCK_TIMEOUT_S


class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if shared.ttfss.fault_checker():
            if shared.ttfss.timer['FAULT_TIMEOUT']:
                return 'FAULT'
        else:
            shared.ttfss.timer['FAULT_TIMEOUT'] = shared.ttfss.UNLOCK_TIMEOUT_S


class update_calculations(GuardStateDecorator):
    def pre_exec(self):
        shared.ttfss.update_calculations()
        return


#############################################
#States

# graph
class INIT(GuardState):
    request = False
    def main(self):
        shared.ttfss.PZT_OUT_LAST = ezca['SQZ-FIBR_SERVO_FASTMON']
        return True

    @deco_autocommit
    @update_calculations
    def run(self):
        return True


#exists only to inform the manager to move into its managed state
class MANAGED(GuardState):
    request = True

    @deco_autocommit
    @update_calculations
    def main(self):
        return True

    @deco_autocommit
    @update_calculations
    def run(self):
        return True


class IDLE(GuardState):

    def main(self):
        ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 0

    @deco_autocommit
    #@fault_checker
    @update_calculations
    def run(self):
        return True


class LOCKLOSS(GuardState):
    """Record lockloss events"""
    request = False
    redirect = False

    @deco_autocommit
    @update_calculations
    def main(self):
        down_state_set()
        #TODO, don't apply lockloss counter logic while managed

        # Comment from Chris:
        # is this todo what we want? I think lockloss counter
        # logic is important for the manager to know
        # when to close beam diverter.

        # count lock loss
        shared.ttfss.LOCK_ATTEMPT_COUNTER += 1
        log("LOCK ATTEMPT " + str(shared.ttfss.LOCK_ATTEMPT_COUNTER))

        if shared.ttfss.LOCK_ATTEMPT_COUNTER > shared.ttfss.LOCK_ATTEMPT_MAX:
            shared.ttfss.LOCK_ATTEMPT_COUNTER = 0
            shared.ttfss.intervention_msg = "Too Many Lock Failures"
            return 'REQUIRES_INTERVENTION'
        return True


class REQUIRES_INTERVENTION(GuardState):
    request = False
    redirect = False

    @update_calculations
    def run(self):
        if shared.ttfss.intervention_msg is not None:
            notify(shared.ttfss.intervention_msg)
        notify("request non-locking state")
        if ezca['GRD-SQZ_TTFSS_REQUEST'] in lock_states:
            return False
        else:
            shared.ttfss.intervention_msg = None
            return True


# reset everything to the good values for acquistion.  These values
# are stored in the down script.
class DOWN(GuardState):
    index   = 1
    request = False
    goto    = True

    @fault_checker
    @update_calculations
    def main(self):
        down_state_set()
        self.timer['down_settle'] = 1
        return

    @fault_checker
    @update_calculations
    def run(self):
        if self.timer['down_settle']:
          return True


class FAULT(GuardState):
    redirect = False
    request = False

    @update_calculations
    def main(self):
        down_state_set()


    @update_calculations
    def run(self):
        blocked = shared.ttfss.fault_checker()
        if blocked is None:
            return
        if not blocked:
            return True

class SLOW_TRACK(GuardState):
    @fault_checker
    @update_calculations
    def main(self):
        down_state_set()
        ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 0

        ezca['SQZ-LASER_HEAD_CRYSTALFREQUENCY'] = ezca['SQZ-TTFSS_GRD_SLOW_TARGET']
        
        self.timer['waiting'] = 20
        self.target_freq = ezca['SQZ-FREQ_DOUBLELASERVCO']/1e6
        self.target_freq_H = self.target_freq + 2.5
        self.target_freq_L = self.target_freq - 2.5
        return

    @fault_checker
    @update_calculations
    def run(self):
        beat_freq = ezca['SQZ-FIBR_LOCK_BEAT_FREQUENCY']
        crystal_freq = ezca['SQZ-LASER_HEAD_CRYSTALFREQUENCY']

        if not self.timer['waiting']:
            return

        ### This might make the beat freq move in the wrong direction as you don't know which side of 0 you are on.
        #TODO: set up beat note scan
        if beat_freq < self.target_freq_L or beat_freq > self.target_freq_H:
            delta_freq = min([self.target_freq-beat_freq,30])
            ezca['SQZ-LASER_HEAD_CRYSTALFREQUENCY'] = crystal_freq + delta_freq
            self.timer['waiting'] = abs(delta_freq / 1.)

        else:
            return True
      

class LOCKING(GuardState):
    index = 5
    @fault_checker
    @update_calculations
    def main(self):
        ezca['SQZ-FIBR_SERVO_COMGAIN'] = ezca['SQZ-TTFSS_GRD_COMGAIN_ACQ']
        ezca['SQZ-FIBR_SERVO_FASTGAIN'] = 20 #BK250124 was 16 set to 19 #AJM20240507 was 14 set to 16, while debugging issue # was 7 in O4a
        ezca['SQZ-FIBR_SERVO_BOOST1EN'] = 1
        ezca['SQZ-FIBR_SERVO_BOOST2EN'] = 0
        ezca['SQZ-FIBR_SERVO_BOOST3EN'] = 0


        self.fast_thr = 0.2 # threshold for fast output
        self.eom_thr = 0.1 # EOM output threshold for gaining
        
        self.target_freq = ezca['SQZ-FREQ_DOUBLELASERVCO']/1e6
        self.target_freq_H = self.target_freq + 20
        self.target_freq_L = self.target_freq - 20
        
        self.timer['lock_settle'] = 0
        self.counter = 0
        self.fail_counter = 0
        return

    @fault_checker
    @update_calculations
    def run(self):
        if self.timer['lock_settle']:
            if self.counter == 0:
                # egnage servo
                ezca['SQZ-FIBR_SERVO_RAMPEN'] = 0
                self.timer['lock_settle'] = 1
                self.counter += 1

            elif self.counter == 1:
                # engage slow loop
                ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 'On'
                ezca['SQZ-FIBR_LOCK_LOGIC_ENABLE'] = 0
                self.timer['notify'] = 20
                self.counter += 1

            elif self.counter == 2:
                # wait the fast output gets lower than threshold, and engage fast int
                if abs(ezca['SQZ-FIBR_SERVO_FASTMON']) < self.fast_thr:
                    ezca['SQZ-FIBR_SERVO_BOOST3EN'] = 1
                    self.timer['lock_settle'] = 0.5
                    self.timer['notify'] = 20                    
                    self.counter += 1
                elif self.timer['notify']:
                    notify('FAST output is stll larger than %f, and fast int cannot be engaged'%self.fast_thr)
                    if ezca['SQZ-FIBR_LOCK_BEAT_FREQUENCY'] < self.target_freq_L or ezca['SQZ-FIBR_LOCK_BEAT_FREQUENCY'] > self.target_freq_H:
                        notify('Frequency is out of range. Check target frequency.')
                        return 'SLOW_TRACK'

                
            elif self.counter == 3:
                # engage eom path
                if abs(ezca['SQZ-FIBR_SERVO_FASTMON']) < self.fast_thr:
                    ezca['SQZ-FIBR_SERVO_EOMEN'] = 1
                    self.timer['EOM_reset'] = 10
                    self.counter += 1
                elif self.timer['notify']:
                    notify('FAST output is stll larger than %f, and EOM path cannot be engaged'%self.fast_thr)
                
            elif self.counter == 4:
                # check EOM output
                if ezca['SQZ-FIBR_SERVO_EOMRMSMON'] < self.eom_thr:
                    self.counter += 1
                    
                elif self.timer['EOM_reset']:                
                    if self.fail_counter > 10:
                        notify('too many failure, go to REQUIRES_INTERVENTION state')
                        return 'REQUIRES_INTERVENTION'
                    notify('EOM output is too large, reset EOM path')
                    ezca['SQZ-FIBR_SERVO_EOMEN'] = 'Off'
                    time.sleep(0.5)
                    ezca['SQZ-FIBR_SERVO_EOMEN'] = 'On'                    
                    self.fail_counter += 1
                    self.timer['EOM_reset'] = 10

            elif self.counter == 5:                
                if ezca['SQZ-FIBR_SERVO_COMGAIN'] + 2 < ezca['SQZ-TTFSS_GRD_COMGAIN_LOCK']:
                    ezca['SQZ-FIBR_SERVO_COMGAIN'] += 2
                    self.timer['lock_settle'] = 0.1
                elif ezca['SQZ-FIBR_SERVO_COMGAIN'] != ezca['SQZ-TTFSS_GRD_COMGAIN_LOCK']:
                    ezca['SQZ-FIBR_SERVO_COMGAIN'] = ezca['SQZ-TTFSS_GRD_COMGAIN_LOCK']
                    self.timer['lock_settle'] = 0.1
                else:
                    self.timer['lock_settle'] = 0.1                    
                    self.counter += 1

                
            elif self.counter == 6:
                ezca['SQZ-FIBR_SERVO_BOOST2EN'] = 1
                time.sleep(2)
                #ezca['SQZ-FIBR_SERVO_BOOST1EN'] = 0
                #time.sleep(1)
                self.counter += 1
                self.timer['freq_check'] = 2

            elif self.counter == 7:
                if abs(ezca['SQZ-FIBR_LOCK_BEAT_FREQUENCY'] - self.target_freq) < 2:
                    if self.timer['freq_check']:
                        self.counter += 1
                else:
                    if self.timer['freq_check']:
                        notify('far from the target frequency. go back to slow track state')
                        return 'SLOW_TRACK'
                
                
            else:
                return True


class LOCKED(GuardState):
    index = 10
    @fault_checker
    @lock_checker
    @update_calculations
    def main(self):
        shared.ttfss.PZT_OUT_LAST = ezca['SQZ-FIBR_SERVO_FASTMON']
        self.timer['lock_settle'] = 1
        self.target_freq = ezca['SQZ-LASER_HEAD_CRYSTALFREQUENCY']
        

    @fault_checker
    @lock_checker
    @update_calculations
    def run(self):
        # reset counter
        if shared.ttfss.LOCK_ATTEMPT_COUNTER:
            shared.ttfss.LOCK_ATTEMPT_COUNTER = 0

        if self.timer['lock_settle']:
            ezca['SQZ-TTFSS_GRD_SLOW_TARGET'] = self.target_freq
            self.timer['lock_settle'] = 1800 #3     #No need to update this often
            self.target_freq = ezca['SQZ-LASER_HEAD_CRYSTALFREQUENCY']
        
        return True
'''
# performed in beckhoff
class LOCKED_and_SLOW(GuardState):
    PZT_OUT_avg = 0
    slow_cycle_s = 10
    grd_cycle_s = 1/16.

    @fault_checker
    @lock_checker
    @update_calculations
    def main(self):
        self.timer['slow_cycle'] = 1
        #preload the PZT out without the averaging, to reduce the transient
        self.PZT_OUT_avg = ezca['SQZ-FIBR_SERVO_FASTMON']
        return

    @fault_checker
    @lock_checker
    @update_calculations
    def run(self):
        #use exponential averaging
        self.PZT_OUT_avg = ((self.PZT_OUT_avg * self.slow_cycle_s) + (ezca['SQZ-FIBR_SERVO_FASTMON'] * self.grd_cycle_s)) / (self.slow_cycle_s + self.grd_cycle_s)

        #calculate these every cycle, but only apply them every slow_cycle_s
        slow_gain = shared.ttfss.SLOW_GAIN_NOM * ezca['SQZ-TTFSS_GRD_SLOW_GAIN']
        # adjust temperature, but not too much
        delta_slow = -slow_gain * (self.PZT_OUT_avg - ezca['SQZ-TTFSS_GRD_SLOW_TARGET'])

        #notify('delta slow: {:.6f}'.format(delta_slow))

        if self.timer['slow_cycle']:
            # if the timer is done, reset and adjust temperature
            self.timer['slow_cycle'] = self.slow_cycle_s

            if delta_slow > shared.ttfss.SLOW_DELTA_MAX:
              delta_slow = shared.ttfss.SLOW_DELTA_MAX
            elif delta_slow < -shared.ttfss.SLOW_DELTA_MAX:
              delta_slow = -shared.ttfss.SLOW_DELTA_MAX

            new_slow = ezca['SQZ-LASER_HEAD_CRYSTALTEMPERATURE'] + delta_slow
            if new_slow < shared.ttfss.SLOW_MIN:
              ezca['SQZ-LASER_HEAD_CRYSTALTEMPERATURE'] = shared.ttfss.SLOW_MIN
            elif new_slow > shared.ttfss.SLOW_MAX:
              ezca['SQZ-LASER_HEAD_CRYSTALTEMPERATURE'] = shared.ttfss.SLOW_MAX
            else:
              ezca['SQZ-LASER_HEAD_CRYSTALTEMPERATURE'] = new_slow

        return True
'''


##################################################

edges = [
    ('INIT', 'IDLE'),
    ('LOCKLOSS', 'DOWN'),
    ('REQUIRES_INTERVENTION', 'DOWN'),
    ('DOWN', 'LOCKING'),
    ('SLOW_TRACK', 'LOCKING'),
    ('LOCKING', 'LOCKED'),
#    ('LOCKED', 'LOCKED_and_SLOW'),
#    ('LOCKED_and_SLOW', 'LOCKED'),
    ('FAULT', 'IDLE'),
    ('DOWN', 'IDLE'),
]


#list of states which can fail out too many times and freeze in LOCKLOSS_TOOMANY
lock_states = [
    'LOCKING', 'LOCKED', 'LOCKED_and_SLOW',
]

